var timerOn = true;
var timer;
let SESSION_TIMER = "TIMER";

function timerStart(time) {
    this.timer(time);
    $('#btn_send_otp').css("display", "none");
    $('#countdown').css("display", "block");
    $('#btn_submit').attr("disabled", "disabled");
}

function timer(remaining) {
    var m = Math.floor(remaining / 60);
    var s = remaining % 60;

    m = m < 10 ? '0' + m : m;
    s = s < 10 ? '0' + s : s;

    $('#countdown').html("<strong>"+m + ":" +s + "</strong>");
    remaining -= 1;

    if(remaining >= 0 && timerOn) {
        setTimeout(function() {
            timer(remaining);
        }, 1000);
        return;
    }

    if(!timerOn) {
        console.log("time off");
        return;
    }

    this.timerEnd();
}

function timerEnd() {
    alert('Batas Waktu OTP Berakhir !');
	$('#btn_send_otp').hide();
    $('#btn_submit').removeAttr("disabled");
	$('#btn_resend_otp').show();	
    $('#countdown').hide();
}

function timerRestart(time) {
	var highestTimeoutId = setTimeout(";");
	for (var i = 0 ; i < highestTimeoutId ; i++) {
		clearTimeout(i); 
	}
	$('#countdown').val('');
	$('#btn_resend_otp').css("display", "none");
    sessionStorage.removeItem(SESSION_TIMER);
	this.timer(time);
	$('#countdown').css("display", "block");
}

function resVerify (){
	$('#btn_verify').show();
    $('#image_verify').hide();
    $('#failed-notif').hide();
}