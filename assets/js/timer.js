var timerOn = true;
var timer;
let SESSION_TIMER = "TIMER";

function timerStart(time) {
    if (typeof(Storage) !== "undefined") {
        if (sessionStorage.getItem(SESSION_TIMER)) {
            console.log("timer is running")
        } else {
            this.timer(time);
            $('#btn_resend_otp_code').css("display", "block");
            $('#btn_submit').attr("disabled", "disabled");
            sessionStorage.setItem(SESSION_TIMER, true);
        }
    } else {
        console.log('no support');
    }
}

function timer(remaining) {
    var m = Math.floor(remaining / 60);
    var s = remaining % 60;

    m = m < 10 ? '0' + m : m;
    s = s < 10 ? '0' + s : s;

    $('#text_timer').val(m + ":" +s);
    remaining -= 1;

    if(remaining >= 0 && timerOn) {
        setTimeout(function() {
            timer(remaining);
        }, 1000);
        return;
    }

    if(!timerOn) {
        console.log("time off");
        return;
    }

    this.timerEnd();
}

function timerEnd() {
    alert('Batas Waktu OTP Berakhir !');
    $('#btn_submit').removeAttr("disabled");
    sessionStorage.removeItem(SESSION_TIMER);
}

function timerRestart(time) {
	var highestTimeoutId = setTimeout(";");
	
for (var i = 0 ; i < highestTimeoutId ; i++) {
    clearTimeout(i); 
}
	$('#text_timer').val('');
	//$('#btn_submit').removeAttr("disabled");
    sessionStorage.removeItem(SESSION_TIMER);
	this.timer(time);
	//sessionStorage.setItem(SESSION_TIMER, true);
}