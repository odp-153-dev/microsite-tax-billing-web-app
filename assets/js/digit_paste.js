var $inputs = $(".input-otp");
var intRegex = /^\d+$/;

// Prevents user from manually entering non-digits.
$(document).ready(function () {
$inputs.on("input.fromManual", function(){
    if(!intRegex.test($(this).val())){
        $(this).val("");
    }
});
});


// Prevents pasting non-digits and if value is 6 characters long will parse each character into an individual box.
$(document).ready(function () {
$inputs.on("paste", function() {
    var $this = $(this);
    var originalValue = $this.val();
    
    $this.val("");

    $this.one("input.fromPaste", function(){
        $currentInputBox = $(this);
        
        var pastedValue = $currentInputBox.val();
        
        if (pastedValue.length == 6 && intRegex.test(pastedValue)) {
            pasteValues(pastedValue);
        }
        else {
            $this.val(originalValue);
        }

        $inputs.attr("maxlength", 1);

        otpFromTextBox1 = document.getElementById("input_otp1").value;
        otpFromTextBox2 = document.getElementById("input_otp2").value;
        otpFromTextBox3 = document.getElementById("input_otp3").value;
        otpFromTextBox4 = document.getElementById("input_otp4").value;
        otpFromTextBox5 = document.getElementById("input_otp5").value;
        otpFromTextBox6 = document.getElementById("input_otp6").value;
        if((otpFromTextBox1!='') && (otpFromTextBox2!='') && (otpFromTextBox3!='') && (otpFromTextBox4!='') && (otpFromTextBox5!='') && (otpFromTextBox6!='')){
            document.getElementById("btn_verify").disabled = false;
        }else{
            document.getElementById("btn_verify").disabled = true;
        }
    });
    
    $inputs.attr("maxlength", 6);
});
});


// Parses the individual digits into the individual boxes.
function pasteValues(element) {
    var values = element.split("");

    $(values).each(function(index) {
        var $inputBox = $('.input-otp[name="chars[' + (index + 1) + ']"]');
        $inputBox.val(values[index])
    });
};