/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var currentBoxNumber = 0;
var otp = '';

$(document).ready(function () {
    $(".verification-text").keydown(function (e) {
        textboxes = $("input.verification-text");
        currentBoxNumber = textboxes.index(this);

            if (this.value.length == this.getAttribute("maxLength") && e.keyCode != 8 && (e.keyCode > 47 && e.keyCode < 58) && currentBoxNumber != 5) {
                if (textboxes[currentBoxNumber + 1] != null) {
                    nextBox = textboxes[currentBoxNumber + 1];
                    nextBox.focus();
                    nextBox.select();
                }
            }
            if (e.keyCode == 8) {
                if (this.value == '') {
                    prevBox = textboxes[currentBoxNumber - 1];
                    prevBox.focus();
                    prevBox.select();
                }
            }
    });
});

$(document).ready(function () {
    $(".verification-text").bind('input', function () {
        console.log(this);
        otpFromTextBox1 = document.getElementsByClassName("input_otp1").value;
        otpFromTextBox2 = document.getElementsByClassName("input_otp2").value;
        otpFromTextBox3 = document.getElementsByClassName("input_otp3").value;
        otpFromTextBox4 = document.getElementsByClassName("input_otp4").value;
        otpFromTextBox5 = document.getElementsByClassName("input_otp5").value;
        otpFromTextBox6 = document.getElementsByClassName("input_otp6").value;
        if((otpFromTextBox1!='') && (otpFromTextBox2!='') && (otpFromTextBox3!='') && (otpFromTextBox4!='') && (otpFromTextBox5!='') && (otpFromTextBox6!='')){
            document.getElementById("btn_verify").disabled = false;
        }else{
            document.getElementById("btn_verify").disabled = true;
        }
    });
});

function checkOtp(tmpotp) {
    
	var highestTimeoutId = setTimeout(";");
    textboxes = $("input.verification-text");  
	otp = '';	
    for (var i = 0; i < textboxes.length; i++) {
            otp += textboxes[i].value;
    }
    if (otp == tmpotp) {
        alert('OTP True');

        $('#btn_verify').hide();
        $('#image_verify').show();
		for (var i = 0 ; i < highestTimeoutId ; i++) {
			clearTimeout(i); 
		}
        document.getElementById("btn_verify").disabled = true;
        document.getElementById("image_verify").src="assets/image/approved_icon.png";
        document.getElementById("image_verify").height = "30";
        document.getElementById("image_verify").width = "30"; 
        $('#image_verify').css( { "margin-top" : "25px", "margin-left" : "50px" } );
        document.getElementById("input_otp1").disabled = true;
        document.getElementById("input_otp2").disabled = true;
        document.getElementById("input_otp3").disabled = true;
        document.getElementById("input_otp4").disabled = true;
        document.getElementById("input_otp5").disabled = true;
        document.getElementById("input_otp6").disabled = true;
        document.getElementById("btn_final_pay").disabled = false;
    } else {
        alert('OTP Wrong');
        $('#btn_send_otp').hide();
		$('#countdown').hide();
		for (var i = 0 ; i < highestTimeoutId ; i++) {
			clearTimeout(i); 
		}
	
        $('#btn_resend_otp').show();
        $('#btn_verify').hide();
        $('#image_verify').show();
        $('#failed-notif').show();
        document.getElementById("btn_verify").disabled = true;
        document.getElementById("image_verify").src="assets/image/failed_icon.png";
        document.getElementById("image_verify").height = "30";
        document.getElementById("image_verify").width = "30"; 
        $('#image_verify').css( { "margin-top" : "25px", "margin-left" : "50px" } );
        document.getElementById("input_otp1").value='';
        document.getElementById("input_otp2").value='';
        document.getElementById("input_otp3").value='';
        document.getElementById("input_otp4").value='';
        document.getElementById("input_otp5").value='';
        document.getElementById("input_otp6").value='';
        return false;
    }

    $(document).ready(function () {
    $('#btn_final_pay').click(function () {
        document.getElementById("input_otp1").value='';
        document.getElementById("input_otp2").value='';
        document.getElementById("input_otp3").value='';
        document.getElementById("input_otp4").value='';
        document.getElementById("input_otp5").value='';
        document.getElementById("input_otp6").value='';
        });
    });
}
