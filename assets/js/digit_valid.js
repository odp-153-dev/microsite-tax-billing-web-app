var index = 0;
var a = 0;

$(document).ready(function () {
    $('#btn_pay').click(function () {
        var at = document.getElementById("verificationCode").value;
        if (at.length < 6 || at.length > 6) {
            if(a==0){
            alert('Digit OTP Wrong');
            }
            document.getElementById("verificationCode").value = '';
            return false;
        } else {
            var index = 0;
            var string = 0;
            do {
                var charCode = at.charAt(index);
                if (isNaN(charCode)) {
                    string++;
                }
                index++;
            } while (index < at.length);
            if (string > 0) {
                alert('OTP Must Number');
                document.getElementById("verificationCode").value = '';
                return false;
            } else {
                return true;
            }
        }
    });
});


$(document).ready(function () {
    $('#verificationCode').bind('input', function () {
        
        var string = document.getElementById("verificationCode").value;
        if((string.length)==6){
          $('#verificationCode').css('border', 'solid 2px green');
          document.getElementById("btn_pay").disabled = false;      
        }
        else if(((string.length)==0)||((string.length)==null)){
          $('#verificationCode').css('border', 'solid 1px #ddd');
        }
        else{
          $('#verificationCode').css('border', 'solid 2px red');
          document.getElementById("btn_pay").disabled = true;
        }

    });
});


function checkOtp(otp) {
    var at = document.getElementById("verificationCode").value;
    a=1;
    if (at == otp) {
        alert('OTP True');
        document.getElementById("verificationCode").value = '';
        document.getElementById("btn_pay").disabled = true;
        $('#verificationCode').css('border', 'solid 1px #ddd');
        window.location.href = "success_page.html";
    } else {
        alert('OTP Wrong');
        document.getElementById("verificationCode").value = '';
        document.getElementById("btn_pay").disabled = true;
        $('#verificationCode').css('border', 'solid 1px #ddd');
        return false;
    }
}