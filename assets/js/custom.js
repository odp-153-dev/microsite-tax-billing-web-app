$(document).ready(function () {
    $("#btn_pay").click(function () {
        $('#div_detail').slideToggle('slow');
        $('#div_payment').slideToggle('slow');
    });
    var datalink = GetURLParameter('data');
    if (datalink == "dja") {
        $.ajax({
            type: 'GET',
            url: 'generateidbilling.php?data=' + datalink,
            data: {get_param: 'value'},
            dataType: 'json',
            success: function (data) {
                $('#input_tujuan_institusi').val(data.tujuaninstansi);
                $('#input_kode_cabang').val(data.kodecabang);
                $('#input_tanggal_bayar').val(data.tanggalbayar);
                loading('20%');
                $('#input_bukti').val(data.bukti);
                $('#input_bukti_dja_pay').val(data.bukti);
                $('#input_kode_billing').val(data.kodebilling);
                $('#input_kode_billing_dja_pay').val(data.kodebilling);
                $('#input_tanggal_buku').val(data.tanggalbuku);
                loading('40%');
                $('#input_kl').val(data.kl);
                $('#input_nama_bayar').val(data.namawajibbayar);
                $('#input_status_kerja').val(data.statuskerja);
                loading('60%');
                $('#input_bank_admin').val(data.bankadmin);
                $('#input_eselon').val(data.eselon);
                $('#input_stan').val(data.stan);
                loading('80%');
                $('#input_jumlah_setoran').val(data.jumlah);
                $('#input_terbilang').val(data.terbilang);
                $('#input_total_pembayaran').val(data.total);
                $('#input_jumlah_setoran_dja_pay').val(data.jumlah);
                $('#input_terbilang_dja_pay').val(data.terbilang);
                $('#input_total_pembayaran_dja_pay').val(data.total);
                $('#detail_dja').contents().clone().insertAfter('#div_detail .card-title');
                $('#payment_dja').contents().clone().appendTo('#div_payment .card-body.custom');
                loading('100%');
            }
        });
    } else if (datalink == "djp") {
        $.ajax({
            type: 'GET',
            url: 'generateidbilling.php?data=' + datalink,
            data: {get_param: 'value'},
            dataType: 'json',
            success: function (data) {
                $('#input_tujuan_institusi_djp').val(data.tujuaninstansi);
                $('#input_transaction_djp').val(data.transaction);
                $('#input_alamat_djp').val(data.alamat);
                loading('15%');
                $('#input_mata_anggaran_djp').val(data.mataanggaran);
                $('#input_tanggal_bayar_djp').val(data.tanggalbayar);
                $('#input_jenis_setoran_djp').val(data.jenissetoran);
                loading('30%');
                $('#input_jumlah_setoran_djp').val(data.jumlah);
                $('#input_jumlah_setoran_djp_pay').val(data.jumlah);
                $('#input_tanggal_buku_djp').val(data.tanggalbuku);
                $('#input_npwp_djp').val(data.npwp);
                loading('50%');
                $('#input_nama_bayar_djp').val(data.namawajibbayar);
                $('#input_no_ketetapan_djp').val(data.noketetapan);
                $('#input_mata_uang_djp').val(data.matauang);
                loading('65%');
                $('#input_bukti_djp').val(data.bukti);
                $('#input_kode_billing_djp').val(data.kodebilling);
                $('#input_bukti_djp_pay').val(data.bukti);
                $('#input_kode_billing_djp_pay').val(data.kodebilling);
                $('#input_no_objek_djp').val(data.nomorobjekpajak);
                loading('75%');
                $('#input_terbilang_djp').val(data.terbilang);
                $('#input_terbilang_djp_pay').val(data.terbilang);
                $('#input_stan_djp').val(data.stan);
                $('#input_masa_pajak_djp').val(data.masapajak);
                loading('90%');
                $('#input_kode_cabang_djp').val(data.kodecabang);
                $('#input_bank_admin_djp').val(data.bankadmin);
                $('#input_total_pembayaran_djp').val(data.total);
                $('#input_total_pembayaran_djp_pay').val(data.total);
                $('#detail_djp').contents().clone().insertAfter('#div_detail .card-title');
                $('#payment_djp').contents().clone().appendTo('#div_payment .card-body.custom');
                loading('100%');
            }
        });
    } else if (datalink == "djbc") {
        $.ajax({
            type: 'GET',
            url: 'generateidbilling.php?data=' + datalink,
            data: {get_param: 'value'},
            dataType: 'json',
            success: function (data) {
                $('#input_tujuan_institusi_djbc').val(data.tujuaninstansi);
                $('#input_bukti_djbc').val(data.bukti);
                $('#input_bukti_djbc_pay').val(data.bukti);
                $('#input_tanggal_bayar_djbc').val(data.tanggalbayar);
                loading('20%');
                $('#input_tanggal_buku_djbc').val(data.tanggalbuku);
                $('#input_kode_cabang_djbc').val(data.kodecabang);
                $('#input_stan_djbc').val(data.stan);
                loading('35%');
                $('#input_kode_billing_djbc').val(data.kodebilling);
                $('#input_kode_billing_djbc_pay').val(data.kodebilling);
                $('#input_nama_bayar_djbc').val(data.namawajibbayar);
                $('#input_npwp_djbc').val(data.npwp);
                loading('50%');
                $('#input_jenis_dokumen_djbc').val(data.dokumen);
                $('#input_no_dokumen_djbc').val(data.nodokumen);
                $('#input_tanggal_dokumen_djbc').val(data.tanggaldokumen);
                loading('65%');
                $('#input_jumlah_setoran_djbc').val(data.jumlah);
                $('#input_jumlah_setoran_djbc_pay').val(data.jumlah);
                $('#input_kode_kpbc_djbc').val(data.kodekpbc);
                $('#input_terbilang_djbc').val(data.terbilang);
                $('#input_terbilang_djbc_pay').val(data.terbilang);
                loading('80%');
                $('#input_bank_admin_djbc').val(data.bankadmin);
                $('#input_total_pembayaran_djbc').val(data.total);
                $('#input_total_pembayaran_djbc_pay').val(data.total);
                $('#detail_djbc').contents().clone().insertAfter('#div_detail .card-title');
                $('#payment_djbc').contents().clone().appendTo('#div_payment .card-body.custom');
                loading('100%');
            }
        });
    } else if (datalink == 'paid') {
        $('#detail_djbc').contents().clone().insertAfter('#div_detail .card-title');
        $('#payment_djbc').contents().clone().appendTo('#div_payment .card-body.custom');
        loading('100%');
        modalPaid();
    } else {
        loading('20%');
        loading('40%');
        loading('60%');
        loading('80%');
        loading('100%');
        $('#div_detail_empty').show();
        start();
    }

    $("#btn_final_pay").on('click', function () {
        Swal({
            title: '<strong style="color: red">Memproses Pembayaran Mohon Menunggu</strong>',
            html:
                '<div style="color: #64d6e2; margin: 0 auto;" class="la-line-scale la-2x">\n' +
                '    <div></div>\n' +
                '    <div></div>\n' +
                '    <div></div>\n' +
                '    <div></div>\n' +
                '    <div></div>\n' +
                '</div>\n<br>' +
                '<h6 style="font-size: smaller;">Demi kelancaran transaksi mohon <strong>TIDAK</strong> menutup atau menekan tombol back</h6>',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showConfirmButton: false,
            timer: 3000
        }).then(function () {
            /**
             * Comment - uncomment the function to show different modal
             */
            modalSuccessPay();
            /* modalFailedPay();*/
        });
    })
});

async function start() {
    const {value: fruit} = await Swal({
        title: 'Pilih Metode Pembayaran',
        input: 'select',
        inputOptions: {
            'djbc': 'DJBC',
            'dja': 'DJA',
            'djp': 'DJP',
            'paid': 'Terbayar'
        },
        inputPlaceholder: 'Pilih Metode Pembayaran',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showCancelButton: false,
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value === 'djbc') {
                    redirectWithParameter('djbc');
                } else if (value === 'dja') {
                    redirectWithParameter('dja');
                } else if (value === 'djp') {
                    redirectWithParameter('djp');
                } else if (value == 'paid') {
                    redirectWithParameter('paid')
                } else {
                    resolve('Anda harus memilih metode pembayaran:)')
                }
            })
        }
    });
}

function modalPaid() {
    Swal({
        title: '<strong style="color: red">TRANSAKSI TELAH DIBAYAR</strong>',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showConfirmButton: true,
        confirmButtonText: 'Kembali',
        confirmButtonAriaLabel: 'kembali'
    }).then((result) => {
        close();
    });
}

function modalSuccessPay() {
    Swal({
        title: '<strong style="color: red">Pembayaran BERHASIL!</strong>',
        html:
            '<b>Terimakasih telah menggunakan layanan Bank Mandiri</b><br/>' +
            '<img src="assets/image/accepted_shape.png" style="height: 100px;" /><br/>',
        heightAuto: false,
        showCancelButton: false,
        showCloseButton: false,
        focusConfirm: true,
        confirmButtonText:
            'Selesai',
        confirmButtonAriaLabel: 'Selesai',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: true,
        customClass: 'swal2-actions',
        confirmButtonClass: 'swal2-confirm'
    }).then((result) => {
		redirectWithParameter('paid');
	});
}

function modalFailedPay() {
    Swal({
        title: '<strong style="color: red">Pembayaran GAGAL!</strong>',
        html:
            '<b>Terimakasih telah menggunakan layanan Bank Mandiri</b><br/>' +
            '<img src="assets/image/declined_shape.png" style="height: 100px;" /><br/>' +
            '<span style="font-size: small">Mohon periksa kembali data pembayaran atau menghubungi Mandiri call <b>14000</b>' +
            ' atau <b>+62 21 5299 7777</b></span>',
        heightAuto: false,
        showCancelButton: false,
        showCloseButton: false,
        focusConfirm: true,
        confirmButtonText:
            'Selesai',
        confirmButtonAriaLabel: 'Selesai',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: true,
        customClass: 'swal2-actions',
        confirmButtonClass: 'swal2-confirm'
    }).then((result) => {
		redirectWithParameter('paid');
	});
}

function redirectWithParameter(data) {
    var url = new URL(window.location.href);
    url.searchParams.set('data', data);
    window.location.replace(url.toString());
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return decodeURIComponent(sParameterName[1]);
        }
    }
}

window.onbeforeunload = function (e) {
    e = e || window.event;
    if (e)
        e.returnValue = 'Sure?';

    return 'Sure?';
};

function closetab() {
    var conf = confirm("Are you sure, you want to close this tab?");
    if (conf)
        close();

}

function loading(percent) {
    $('#xx1').hide();
    $('.progress1 span').animate({width: percent}, function () {
        $(this).children().html();
        if (percent == '100%') {
            $('#xx').hide();
            $('#xx1').show();
        }
    })
}