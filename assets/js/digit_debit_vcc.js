(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

//index1

$("#text_no_cvv").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$("#verificationCode").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$("#no_debt_card").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$(document).ready(function () {
    $('#no_debt_card,#text_no_cvv').bind('input', function () {
        var cvvCode = document.getElementById("text_no_cvv").value;
        var noDeptCard = document.getElementById("no_debt_card").value;
        if (document.getElementById($(this).attr('id')).value.length == $(this).attr('maxLength')) {
            $('#' + $(this).attr('id')).css('border', 'solid 2px green');
        }
        else if ((document.getElementById($(this).attr('id')).value.length == 0) || (document.getElementById($(this).attr('id')).value.length == null)) {
            $('#' + $(this).attr('id')).css('border', 'solid 1px #ddd');
        }
        else {
            $('#' + $(this).attr('id')).css('border', 'solid 2px red');
        }
        if ((cvvCode.length == 3) && (noDeptCard.length == 16)) {
            document.getElementById("btn_submit").disabled = false;
        }
        else {
            document.getElementById("btn_submit").disabled = true;
        }
    });
});


$(document).ready(function () {
    $('#btn_submit').click(function () {
        document.getElementById("verificationCode").disabled = false;
    });
});

//index2

$("#input_payment_no_debt").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$("#input_payment_no_cvv").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$("#input_no_phone").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$(document).ready(function () {
    $('#input_payment_no_debt, #input_payment_no_cvv, #input_no_phone').bind('input', function () {
        var noDeptCard = document.getElementById("input_payment_no_debt").value;
        var cvvCode = document.getElementById("input_payment_no_cvv").value;
        var noHandphone = document.getElementById("input_no_phone").value;

        if (document.getElementById($(this).attr('id')).value.length == $(this).attr('maxLength') && $(this).attr('id')!='input_no_phone') {
            $('#' + $(this).attr('id')).css('border', 'solid 2px green');
        }
        else if ( $(this).attr('id')=='input_no_phone' &&  noHandphone.length > 9){
            $('#' + $(this).attr('id')).css('border', 'solid 2px green');
        }
        else if ((document.getElementById($(this).attr('id')).value.length == 0) || (document.getElementById($(this).attr('id')).value.length == null)) {
            $('#' + $(this).attr('id')).css('border', 'solid 1px #ddd');
        }
        else {
            $('#' + $(this).attr('id')).css('border', 'solid 2px red');
        }

        if ((cvvCode.length == 3) && (noDeptCard.length == 16) && (noHandphone.length > 9)) {
            document.getElementById("btn_send_otp").disabled = false;
        }
        else {
            document.getElementById("btn_send_otp").disabled = true;
        }

    });
});

$(document).ready(function () {
    $('#btn_send_otp').click(function () {
        document.getElementById("input_otp1").disabled = false;
        document.getElementById("input_otp2").disabled = false;
        document.getElementById("input_otp3").disabled = false;
        document.getElementById("input_otp4").disabled = false;
        document.getElementById("input_otp5").disabled = false;
        document.getElementById("input_otp6").disabled = false;
    });
});


