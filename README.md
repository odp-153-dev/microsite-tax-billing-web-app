# Microsite Tax Billing - Frontend
> Pembuatan Mircosite dan service untuk pembayaran tagihan DJA, DJP dan DJPB pada Portal SSO

[![NPM Version][npm-image]][npm-url]

## Installation

OS X & Linux:

```sh
1. Open terminal
2. $git clone https://gitlab.com/odp-153-dev/microsite-tax-billing-web-app.git
3. $cd microsite-tax-billing-web-app
4. $npm install
5. double click file index.html or open it using local web server (Apache, MAMP, XAMPP, WAMP)
```

Windows:

```sh
1. buat direktory mikrosite di htdocs
2. clone direktori microsite dari git
2. instal npm di direktori yang sama
3. buka direktori mikrosite dari localhost
```

## Usage example

Microsite untuk menampilkan informasi detail tagihan Wajib Bayar dan service untuk pembayaran tagihan DJA, DJP dan DJPB pada portal SSO milik Kementerian Keuangan. 

## Development setup

Describe how to install all development dependencies and how to run an automated test-suite of some kind. Potentially do this for multiple platforms.

```sh
npm install
```

## Git
```
Clone it (https://gitlab.com/odp-153-dev/microsite-tax-billing-web-app.git)
```
<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics