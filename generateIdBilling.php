<?php
	
	$data = $_GET['data'];
	
	$object = new stdClass();
	if($data == "dja") {		
		$object->kodebilling = "1234567";
		$object->tujuaninstansi = "50012/Pajak/PNPB/Cukai";
		$object->kodecabang = "00008";
		$object->tanggalbayar = "24/10/2018 16:18:44";
		$object->bukti = "Penerimaan Negara Bukan Pajak";
		$object->tanggalbuku = "24/10/2018";
		$object->kl = "Kementerian Keuangan";
		$object->namawajibbayar = "Arika Saputro";
		$object->statuskerja = "Pegawai Swasta";
		$object->bankadmin = "IDR 5.000,00";
		$object->eselon = "Dirjen Bea Cukai";
		$object->stan = "2901809";
		$object->jumlah = "Rp. 8.591.764,00";
		$object->terbilang = "Delapan juta lima ratus sembilan puluh satu ribu tujuh ratus enam puluh empat rupiah";
		$object->total = "Rp. 8.596.764,00";
	}
	else if($data=="djp"){
		$object->transaction = "201810241625110135";
		$object->tujuaninstansi = "50012/Pajak/PNPB/Cukai";
		$object->alamat = "SAMBIROTO No 2 RT 006 RW 003, PURWOMARTANI, SLEMAN";
		$object->kodebilling = "118100753693111";
		$object->mataanggaran = "411121";
		$object->tanggalbayar = "24/10/2018 16:18:44";
		$object->kodecabang = "008";
		$object->jenissetoran = "100";
		$object->jumlah = "Rp. 5.000,00";
		$object->npwp = "71498668454200";
		$object->tanggalbuku = "24/10/2018";
		$object->namawajibbayar = "Arika Saputro";
		$object->noketetapan = "00000000000000";
		$object->matauang = "IDR";
		$object->bukti = "Penerimaan Pajak";
		$object->nomorobjekpajak = "-";
		$object->stan = "6044330";
		$object->masapajak = "09092018";
		$object->terbilang = "Lima ribu rupiah";
		$object->bankadmin = "IDR. 0,00";
		$object->total = "IDR. 5.000,00";
	}
	else if($data=="djbc"){
		$object->tujuaninstansi = "50012/Pajak/PNPB/Cukai";
		$object->bukti = "Penerimaan Bea Dan Cukai";
		$object->tanggalbayar = "24/10/2018 16:18:44";
		$object->tanggalbuku = "24/10/2018";
		$object->kodecabang = "00008";
		$object->stan = "409727";
		$object->kodebilling = "620180800067571";
		$object->namawajibbayar = "PT. BERDIKARI";
		$object->npwp = "71498668454200";
		$object->dokumen = "01";
		$object->nodokumen = "00000000702620180807002733";
		$object->tanggaldokumen = "2018/08/09";
		$object->jumlah = "Rp. 148.959.000,00";
		$object->kodekpbc = "-";
		$object->terbilang = "Seratus empat puluh delapan juta sembilan ratus lima puluh sembilan ribu rupiah";
		$object->bankadmin = "IDR. 0,00";
		$object->total = "Rp. 148.959.000,00";
	}
	print json_encode($object);
	
	die();
	
?>